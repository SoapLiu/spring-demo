package org.soap.dao;

import org.soap.pojo.User;

import java.util.List;

/**
 * @author liuxingtai
 * @create 2021-07-05-0:14
 * @site https://gitee.com/SoapLiu
 */
public interface IUserDao {
    //查询所有用户
    public List<User> findAll() throws Exception;


    //根据条件进行用户查询
    public User findByCondition(User user) throws Exception;
}

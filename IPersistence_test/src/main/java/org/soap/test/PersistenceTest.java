package org.soap.test;

import org.junit.Test;
import org.soap.dao.IUserDao;
import org.soap.io.Resources;
import org.soap.pojo.User;
import org.soap.sqlSession.SqlSession;
import org.soap.sqlSession.SqlSessionFactory;
import org.soap.sqlSession.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

/**
 * @author liuxingtai
 * @create 2021-07-04-10:25
 * @site https://gitee.com/SoapLiu
 */
public class PersistenceTest {

    @Test
    public void test() throws Exception{
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //创建对象实体
        User user = new User();
        user.setId(1);
        user.setUsername("张三");
//        User user1 = sqlSession.selectOne("user.selectOne",user);
//        List<User> list = sqlSession.selectList("user.selectList");
//        for (User user1 : list) {
//            System.out.println(user1);
//        }
        IUserDao mapper = sqlSession.getMapper(IUserDao.class);
        List<User> all = mapper.findAll();
        for (User user1 : all) {
            System.out.println(user1);
        }
    }
}

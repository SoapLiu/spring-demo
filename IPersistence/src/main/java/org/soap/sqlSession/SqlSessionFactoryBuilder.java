package org.soap.sqlSession;

import org.dom4j.DocumentException;
import org.soap.config.XMLConfigBuilder;
import org.soap.pojo.Configuration;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * @author liuxingtai
 * @create 2021-07-04-11:28
 * @site https://gitee.com/SoapLiu
 */
public class SqlSessionFactoryBuilder {

    public SqlSessionFactory build(InputStream inputStream) throws PropertyVetoException, DocumentException {
        //第一：使用dom4j解析配置文件，将解析出来的内容封装到Configuration中
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parseConfig(inputStream);
        //第二：创建SqlSessionFactory对象
        DefaultSqlSessionFatory defaultSqlSessionFatory = new DefaultSqlSessionFatory(configuration);
        return defaultSqlSessionFatory;
    }
}

package org.soap.sqlSession;

import org.soap.pojo.Configuration;
import org.soap.pojo.MappedStatement;

import java.sql.SQLException;
import java.util.List;

/**
 * @author liuxingtai
 * @create 2021-07-04-13:07
 * @site https://gitee.com/SoapLiu
 */
public interface Executor {
    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement,Object... params) throws SQLException, Exception;
}

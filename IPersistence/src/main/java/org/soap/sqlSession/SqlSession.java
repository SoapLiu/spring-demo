package org.soap.sqlSession;


import java.util.List;

/**
 * @author liuxingtai
 * @create 2021-07-04-12:30
 * @site https://gitee.com/SoapLiu
 */
public interface SqlSession {
    //查询所有
    public <E> List<E> selectList(String statmentid,Object... params) throws Exception;

    //查询单个
    public <T> T selectOne(String statmentid,Object... params) throws Exception;

    //为Dao接口生成代理实现类
    public <T> T getMapper(Class<?> mapperClass);
}

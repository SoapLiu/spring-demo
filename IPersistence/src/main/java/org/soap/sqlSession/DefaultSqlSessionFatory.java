package org.soap.sqlSession;

import org.soap.pojo.Configuration;

/**
 * @author liuxingtai
 * @create 2021-07-04-12:28
 * @site https://gitee.com/SoapLiu
 */
public class DefaultSqlSessionFatory implements SqlSessionFactory{
    private Configuration configuration;

    public DefaultSqlSessionFatory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}

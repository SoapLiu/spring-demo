package org.soap.sqlSession;

/**
 * @author liuxingtai
 * @create 2021-07-04-11:27
 * @site https://gitee.com/SoapLiu
 */
public interface SqlSessionFactory {
    public SqlSession openSession();
}

package org.soap.io;

import java.io.InputStream;

/**
 * @author liuxingtai
 * @create 2021-07-04-10:21
 * @site https://gitee.com/SoapLiu
 */
public class Resources {
    // 根据配置文件的路径，将配置文件加载成字节输入流，存储在内存中
    // 根据配置文件的路径，将配置文件加载成字节输入流，存储在内存中
    public static InputStream getResourceAsSteam(String path){
        InputStream resourceAsStream = Resources.class.getClassLoader().getResourceAsStream(path);
        return  resourceAsStream;

    }
}

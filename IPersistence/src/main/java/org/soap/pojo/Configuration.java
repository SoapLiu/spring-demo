package org.soap.pojo;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liuxingtai
 * @create 2021-07-04-10:33
 * @site https://gitee.com/SoapLiu
 */
public class Configuration {
    private DataSource dataSource;
    /**
     * key：statementid   value：封装好的MappedStatement对象
     */
    Map<String,MappedStatement> map = new HashMap<>();

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MappedStatement> getMap() {
        return map;
    }

    public void setMap(Map<String, MappedStatement> map) {
        this.map = map;
    }
}
